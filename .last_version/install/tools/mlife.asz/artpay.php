<?define("NOT_CHECK_PERMISSIONS", true);?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

if(\Bitrix\Main\Loader::IncludeModule("mlife.asz")){
	$json = file_get_contents('php://input');
	//$json = '{"ap_storeid":"600899","ap_order_num":147,"ap_test":1,"ap_notice_type":"PayOrder","ap_operation":"Sale","ap_amount":58.8,"ap_currency":"BYN","ap_card_mask_num":"445453******0999","ap_trans_dt":"2020-07-15T11:43:50","ap_auth_code":"114357","ap_reference_code":"114357","ap_action_code":"0","up_submit":"\u041e\u043f\u043b\u0430\u0442\u0438\u0442\u044c","ap_signature":"cbfd00d247d5e8c0a4b5285f025650dd14b3e7a2b04274319dd016380afa962ee072a93c63bfe2ed8af268b355776c136b8147759f7ed305372d4f74f9a7df24"}';
	$dataOrder = \Bitrix\Main\Web\Json::decode($json);
	file_put_contents($_SERVER['DOCUMENT_ROOT'].'/order.txt',print_r($json,true),FILE_APPEND);
	
	$orderId = false;
	
	if(isset($dataOrder['ap_order_num']) && intval($dataOrder['ap_order_num'])>0){
		$orderId = intval($dataOrder['ap_order_num']);
	}
	
	if(!$orderId) {
		echo 'Params error';
		die();
	}
	
	$res = \Mlife\Asz\OrderTable::getList(array("select"=>array("*","PAYN_"=>"ADDPAY.*"),"filter"=>array("ID"=>$orderId)));
	if($dataAr = $res->Fetch()){
		//echo '<pre>';print_r($dataAr);echo '</pre>';
		$cl = "\Mlife\\Asz\\Payment\\".$dataAr["PAYN_ACTIONFILE"];
		if($dataAr["PAYN_ACTIONFILE"] && class_exists($cl)){
			$cl::checkPay($dataAr, $dataOrder);
		}else{
			echo 'payment class not found';
		}
	}else{
		echo 'Order not found';
	}
}

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");
?>