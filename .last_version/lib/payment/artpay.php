<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.asz
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Asz\Payment;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/paydefault.php');
Loc::loadMessages(__DIR__.'/payw.php');

//TODO вынести ошибки в языковые файлы

class artpay extends Internals\Paysystem{
	
	//html форма с параметрами обработчика
    public static function showParamsForm($str_PARAMS){
		
		$paramArray = self::getParamsArray($str_PARAMS);

        $html = self::prepareHtml($str_PARAMS);
		
		$html .= '<tr>
		<td>Режим работы обработчика</td>
		<td>';
        $html .= '<select name="TEST_MODE">';
        $selected = ($paramArray['TEST_MODE']=='') ? " selected=selected" : "";
        $html .= '<option value=""'.$selected.'>Боевой режим</option>';
        $selected = ($paramArray['TEST_MODE']=='Y') ? " selected=selected" : "";
        $html .= '<option value="Y"'.$selected.'>Тестовый режим</option>';
        $html .= '</select>';
        $html .= '</tr>';
		
		$html .= '<tr>
		<td>Идентификатор точки обслуживания.</td>
		<td><input name="ART_userName" type="text" value="'.$paramArray['ART_userName'].'"/></td>
		</tr>';
		
		$html .= '<tr>
		<td>Код валюты в магазине (BYN|USD|EUR|RUB).</td>
		<td><input name="ART_ap_currency" type="text" value="'.$paramArray['ART_ap_currency'].'"/></td>
		</tr>';
		
		$html .= '<tr>
		<td>Ключевая фраза для подписи запросов магазина.</td>
		<td><input name="ART_key1" type="text" value="'.$paramArray['ART_key1'].'"/></td>
		</tr>';
		
		$html .= '<tr>
		<td>Ключевая фраза для подписи ответов шлюза.</td>
		<td><input name="ART_key2" type="text" value="'.$paramArray['ART_key2'].'"/></td>
		</tr>';
		
		
		
		return $html;
		
	}
	
	public static function onSendParamsForm(){

        $arParams = self::prepareOnSendParamsForm();

        $arParams['TEST_MODE'] = $_REQUEST['TEST_MODE'] ? "Y" : "";
		$arParams['ART_userName'] = trim($_REQUEST['ART_userName']) ? trim($_REQUEST['ART_userName']) : "";
		$arParams['ART_ap_currency'] = trim($_REQUEST['ART_ap_currency']) ? trim($_REQUEST['ART_ap_currency']) : "";
		$arParams['ART_key1'] = trim($_REQUEST['ART_key1']) ? trim($_REQUEST['ART_key1']) : "";
		$arParams['ART_key2'] = trim($_REQUEST['ART_key2']) ? trim($_REQUEST['ART_key2']) : "";
        

        return serialize($arParams);

    }
	
	//получение кнопки для оплаты
	public static function getPayButton($orderId){
		
		if(!$orderId) return '';
		
		$res = \Mlife\Asz\OrderTable::getList(array("select"=>array("*"),"filter"=>array("ID"=>$orderId)));
        if($dataAr = $res->Fetch()){
			
			$paymentId = $dataAr["PAY_ID"];
            $paramArray = self::getParamsFromBase($paymentId);

            if($paramArray['ASZ_STATUS']=='all'){
                $right = true;
            }else{
                if($dataAr['STATUS']){
                    if(in_array($dataAr['STATUS'],explode(',',$paramArray['ASZ_STATUS']))) {
                        $right = true;
                    }else{
                        $right = false;
                    }
                }else{
                    $right = false;
                }
            }
            if(!$right) return "";
			
			$url = 'https://engine.artpay.by/create/';
			//$url2 = 'https://api.artpay.by/v2/';
			if($paramArray['TEST_MODE']){
				$url = 'https://gateway-sandbox-artpay.dev-3c.by/create/';
				//$url2 = 'https://api-sandbox-artpay.dev-3c.by/v2/';
			}
			
			$price = \Mlife\Asz\CurencyFunc::convertFromBase($dataAr["PRICE"],$paramArray["ART_ap_currency"],$dataAr["SITEID"]);
			$price = number_format($price,2,".","");
			
			$arFields = array(
				'ap_storeid'=>$paramArray['ART_userName'],
				'ap_order_num'=>$dataAr['ID'],
				'ap_amount'=>$price,
				'ap_currency'=>$paramArray["ART_ap_currency"],
				'ap_client_dt'=>time(),
				'ap_invoice_desc'=>"Оплата заказа номер ".$dataAr['ID'],
				'up_submit'=>'Оплатить'
			);
			if($paramArray['TEST_MODE']){
				$arFields['ap_test'] = 1;
			}else{
				$arFields['ap_test'] = 0;
			}
			
			//$arFields['ap_request'] = 'Register';
			//$arFields['ap_operation'] = 'Sale';
			//$arFields['ap_proto_ver'] = '1.3.0';
			
			$string = '';
			//ksort($arFields,SORT_STRING);
			uksort($arFields, 'strnatcmp');
			//echo'<pre>';print_r($arFields);echo'</pre>';
			foreach ($arFields as $param => $value){
				$string .= $value . ';';
			}

			$string .=  trim($paramArray["ART_key1"]);
			//echo'<pre>';print_r($string);echo'</pre>';
			$hash =  hash('sha512', $string);
			$arFields['ap_signature'] = $hash;
			//echo'<pre>';print_r($arFields['ap_signature']);echo'</pre>';
			
			$html = '<form target="_blank" method="POST" action="'.$url.'" charset="utf-8">';
			foreach($arFields as $key=>$val){
				if($key=='up_submit') continue;
				$html .= '<input type="hidden" name="'.$key.'" value="'.$val.'"/>';
			}
			$html .= '<input class="btn btn-primary" name="up_submit" type="submit" value="'.$arFields['up_submit'].'"/>';
			$html .= '<form>';
			
			return $html;
		}
		
	}
	
	public static function checkPay($order, $data){
		
		//echo'<pre>';print_r($data);echo'</pre>';
		//die();
		
		$paymentId = $order["PAY_ID"];
		
		$paramArray = self::getParamsFromBase($paymentId);
		
		
		$addSignature = $data['ap_signature'];
		unset($data['ap_signature']);

		uksort($data, 'strnatcmp');
		$string =  implode(';', $data) . ';' . trim($paramArray["ART_key2"]);
		
		if(hash("sha512", $string) == $addSignature){
			
			if($data['ap_trans_dt']){
				if($paramArray["ASZ_STATUS_S"]>0){
					//обновление статуса
					$res = \Mlife\Asz\OrderTable::update($order["ID"],array("STATUS"=>$paramArray["ASZ_STATUS_S"]));
				}else{
					
				}
			}
			
		}else{
			echo 'incorrect signature';
		}
		
	}
	
}