<?
namespace Mlife\Asz\Payment\Internals;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Paysystem {

    protected static $row = false; //статическая переменная, сюда кешируем параметры

    public static function getPaymentData($paymentId){
        if(!isset(self::$row[$paymentId])) {
            $res = \Mlife\Asz\PaysystemTable::getRowById($paymentId);
            self::$row[$paymentId] = $res;
        }
        return self::$row[$paymentId];
    }

    public static function prepareHtml($str_PARAMS){

        $paramArray = self::getParamsArray($str_PARAMS);

        $arDelivery = self::getDeliveryList();
        $activeDelivery = array();
        if($paramArray['delivery'] && $paramArray['delivery']!='all'){
            $activeDelivery = explode(",",$paramArray['delivery']);
        }

        $arStatus = self::getStatusList();
        $activeStatus = array();
        if($paramArray['ASZ_STATUS'] && $paramArray['ASZ_STATUS']!='all'){
            $activeStatus = explode(",",$paramArray['ASZ_STATUS']);
        }

        $html = "";
        $html .= "<tr><td>".Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_LOGO")."</td><td>";
        $html .= \CAdminFileDialog::ShowScript(array(
            "event" => "image2",
            "arResultDest" => array("ELEMENT_ID" => "image"),
            "arPath" => array("PATH" => GetDirPath($paramArray['image'])),
            "select" => 'F',// F - file only, D - folder only
            "operation" => 'O',// O - open, S - save
            "showUploadTab" => true,
            "showAddToMenuTab" => false,
            "fileFilter" => 'jpg,jpeg,png,gif',
            "allowAllFiles" => false,
            "SaveConfig" => true,
        ));
        $html .=	'<input
						name="image"
						id="image"
						type="text"
						value="'.htmlspecialcharsbx($paramArray['image']).'"
						size="35">&nbsp;<input
						type="button"
						value="..."
						onClick="image2()"
					>';
        $html .= "</td></tr>";
        $html .= '<tr><td>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_SUMM").'</td><td>';
        $html .= '<input name="summ1" type="text" value="'.$paramArray['summ1'].'"/> - <input type="text" name="summ2" value="'.$paramArray['summ2'].'"/>';
        $html .= '</td></tr>';
        $html .= '<tr><td>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_TAX").'</td><td>';
        $html .= '<input name="tax" type="text" value="'.$paramArray['tax'].'"/>';
        $html .= '</td></tr>';

        $html .= '<tr><td>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_DELIVERY").'</td><td>';
        $html .= '<select name="delivery[]" multiple="multiple">';
        $selected = ($paramArray['delivery']=='all') ? " selected=selected" : "";
        $html .= '<option value="all"'.$selected.'>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_DELIVERY_DEF").'</option>';
        foreach($arDelivery as $key=>$val){
            $selected = (in_array($key,$activeDelivery)) ? " selected=selected" : "";
            $html .= '<option value="'.$key.'"'.$selected.'>'.$val.'</option>';
        }
        $html .= '</select>';
        $html .= '</td></tr>';

        $html .= '<tr>
		<td>'.Loc::getMessage("MLIFE_ASZ_PAYW1_PARAM10").'</td>
		<td>';
        $html .= '<select name="ASZ_STATUS[]" multiple="multiple">';
        $selected = ($paramArray['ASZ_STATUS']=='all') ? " selected=selected" : "";
        $html .= '<option value="all"'.$selected.'>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_STATUS_DEF").'</option>';
        foreach($arStatus as $key=>$val){
            $selected = (in_array($key,$activeStatus)) ? " selected=selected" : "";
            $html .= '<option value="'.$key.'"'.$selected.'>'.$val.'</option>';
        }
        $html .= '</select>';
        $html .= '</tr>';

        $html .= '<tr>
		<td>'.Loc::getMessage("MLIFE_ASZ_PAYW1_PARAM11").'</td>
		<td>';
        $html .= '<select name="ASZ_STATUS_S">';
        $selected = ($paramArray['ASZ_STATUS_S']=='') ? " selected=selected" : "";
        $html .= '<option value=""'.$selected.'>'.Loc::getMessage("MLIFE_ASZ_PAYDEFAULT_STATUS_DEF2").'</option>';
        foreach($arStatus as $key=>$val){
            $selected = ($key==$paramArray["ASZ_STATUS_S"]) ? " selected=selected" : "";
            $html .= '<option value="'.$key.'"'.$selected.'>'.$val.'</option>';
        }
        $html .= '</select>';
        $html .= '</tr>';

        return $html;

    }

    public static function prepareOnSendParamsForm(){

        $arParams = array();
        if($_REQUEST['image']) $arParams['image'] = $_REQUEST['image'];
        if($_REQUEST['summ1']) $arParams['summ1'] = $_REQUEST['summ1'];
        if($_REQUEST['summ2']) $arParams['summ2'] = $_REQUEST['summ2'];
        if($_REQUEST['tax']) $arParams['tax'] = $_REQUEST['tax'];

        if($_REQUEST['delivery']) {
            if(is_array($_REQUEST['delivery'])) {
                if(in_array('all',$_REQUEST['delivery'])) {
                    $arParams['delivery'] = 'all';
                }else{
                    $arParams['delivery'] = implode(",",$_REQUEST['delivery']);
                }
            }
        }else{
            $arParams['delivery'] = 'all';
        }

        if($_REQUEST['ASZ_STATUS']) {
            if(is_array($_REQUEST['ASZ_STATUS'])) {
                if(in_array('all',$_REQUEST['ASZ_STATUS'])) {
                    $arParams['ASZ_STATUS'] = 'all';
                }else{
                    $arParams['ASZ_STATUS'] = implode(",",$_REQUEST['ASZ_STATUS']);
                }
            }
        }else{
            $arParams['ASZ_STATUS'] = 'all';
        }
        if($_REQUEST['ASZ_STATUS_S']) $arParams['ASZ_STATUS_S'] = intval($_REQUEST['ASZ_STATUS_S']);

        return $arParams;

    }

    //стоимость данного способа оплаты
    public static function getCost($paymentId,$order){

        if(intval($paymentId)>0) {

            $paramArray = self::getParamsFromBase($paymentId);

            if($paramArray['tax']>0) {
                $cost = round((($order['ITEMSUMFIN'] * $paramArray['tax']) / 100),2);
            }else{
                $cost = 0;
            }

            return $cost;

        }

        return '0';

    }

    //права на вывод данного способо оплаты
    public static function getRight($paymentId,$order) {

        $paramArray = self::getParamsFromBase($paymentId);

        if($paramArray['summ2']<=0) {
            $right = true;
        }elseif(($paramArray['summ2'] >= $order['ITEMSUMFIN']) && ($paramArray['summ1'] <= $order['ITEMSUMFIN'])) {
            $right = true;
        }else{
            $right = false;
        }

        //проверка привязок служб доставки
        if($right) {
            if($paramArray['delivery']=='all'){
                $right = true;
            }else{
                if($order['DELIVERY_ID']){
                    if(in_array($order['DELIVERY_ID'],explode(',',$paramArray['delivery']))) {
                        $right = true;
                    }else{
                        $right = false;
                    }
                }else{
                    $right = true;
                }
            }
        }

        return $right;

    }

    //возвращает иконку платежки
    public static function getImage($paymentId){

        $paramArray = self::getParamsFromBase($paymentId);

        return $paramArray['image'];

    }

    public static function getParamsFromBase($paymentId) {
        $payData = self::getPaymentData($paymentId);
        $paramArray = self::getParamsArray($payData['PARAMS']);
        return $paramArray;
    }

    //получение параметров из сериализованной строки
    public static function getParamsArray($str_PARAMS) {

        $paramArray = array(
            "image" => "/bitrix/images/mlife/asz/deliver.png",
            "summ1" => "0",
            "summ2" => "0",
            "tax" => "0",
            "delivery" => "all",
        );

        $paramBase = unserialize($str_PARAMS);
        foreach($paramBase as $k=>$v){
            $paramArray[$k] = $v;
        }

        return $paramArray;

    }

    public static function getDeliveryList(){
        $res = \Mlife\Asz\DeliveryTable::getList(
            array(
                'filter' => array("ACTIVE"=>"Y"),
                'select' => array("ID","NAME","SITEID")
            )
        );
        $arDelivery = array();
        while($arRes = $res->Fetch()){
            $arDelivery[$arRes['ID']] = '['.$arRes['SITEID'].']['.$arRes['ID'].']'.$arRes['NAME'];
        }
        return $arDelivery;
    }

    public static function getStatusList(){
        $res = \Mlife\Asz\OrderStatusTable::getList(
            array(
                'select' => array("ID","NAME","SITEID"),
                'filter' => array("ACTIVE"=>"Y"),
            )
        );
        $arStatus = array();
        while($resAr = $res->Fetch()){
            $arStatus[$resAr["ID"]] = "[".$resAr["SITEID"]."] - ".$resAr["NAME"];
        }
        return $arStatus;
    }


}
