<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.asz
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Asz\Payment;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/paydefault.php');
Loc::loadMessages(__DIR__.'/payw.php');

//TODO вынести ошибки в языковые файлы

class sberkassa extends Internals\Paysystem{

    //html форма с параметрами обработчика
    public static function showParamsForm($str_PARAMS){

        $paramArray = self::getParamsArray($str_PARAMS);

        $html = self::prepareHtml($str_PARAMS);

        $html .= '<tr class="heading"><td colspan="2">Параметры обработчика</td></tr>';

        $html .= '<tr>
		<td>Режим работы обработчика</td>
		<td>';
        $html .= '<select name="TEST_MODE">';
        $selected = ($paramArray['TEST_MODE']=='') ? " selected=selected" : "";
        $html .= '<option value=""'.$selected.'>Боевой режим</option>';
        $selected = ($paramArray['TEST_MODE']=='Y') ? " selected=selected" : "";
        $html .= '<option value="Y"'.$selected.'>Тестовый режим</option>';
        $html .= '</select>';
        $html .= '</tr>';

        $html .= '<tr>
		<td>Фискальные данные</td>
		<td>';
        $html .= '<select name="FISK_MODE">';
        $selected = ($paramArray['FISK_MODE']=='') ? " selected=selected" : "";
        $html .= '<option value=""'.$selected.'>Нет</option>';
        $selected = ($paramArray['FISK_MODE']=='Y') ? " selected=selected" : "";
        $html .= '<option value="Y"'.$selected.'>Да</option>';
        $html .= '</select>';
        $html .= '</tr>';


        $html .= '<tr>
		<td>userName</td>
		<td><input name="SBER_userName" type="text" value="'.$paramArray['SBER_userName'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>password</td>
		<td><input name="SBER_password" type="text" value="'.$paramArray['SBER_password'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>yourSecretToken</td>
		<td><input name="sber_yourSecretToken" type="text" value="'.$paramArray['sber_yourSecretToken'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>returnUrl</td>
		<td><input name="SBER_returnUrl" type="text" value="'.$paramArray['SBER_returnUrl'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>failUrl</td>
		<td><input name="SBER_failUrl" type="text" value="'.$paramArray['SBER_failUrl'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>sessionTimeoutSecs</td>
		<td><input name="SBER_sessionTimeoutSecs" type="text" value="'.$paramArray['SBER_sessionTimeoutSecs'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>customerDetailsPhone</td>
		<td><input name="SBER_customerDetailsPhone" type="text" value="'.$paramArray['SBER_customerDetailsPhone'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>customerDetailsEmail</td>
		<td><input name="SBER_customerDetailsEmail" type="text" value="'.$paramArray['SBER_customerDetailsEmail'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>customerDetailsFullName</td>
		<td><input name="SBER_customerDetailsFullName" type="text" value="'.$paramArray['SBER_customerDetailsFullName'].'"/></td>
		</tr>';
        $html .= '<tr>
		<td>код системного свойства для записи ссылки в заказ</td>
		<td><input name="SBER_systemcode" type="text" value="'.$paramArray['SBER_systemcode'].'"/></td>
		</tr>';

        $html .= '<tr>
		<td>'.Loc::getMessage("MLIFE_ASZ_PAYW1_PARAM7").'</td>
		<td><input name="ASZ_CURRENCY_ID" type="text" value="'.$paramArray['ASZ_CURRENCY_ID'].'"/></td>
		</tr>';

        return $html;
    }

    public static function onSendParamsForm(){

        $arParams = self::prepareOnSendParamsForm();

        $arParams['TEST_MODE'] = $_REQUEST['TEST_MODE'] ? "Y" : "";
        $arParams['FISK_MODE'] = $_REQUEST['FISK_MODE'] ? "Y" : "";
        $arParams['SBER_userName'] = trim($_REQUEST['SBER_userName']) ? trim($_REQUEST['SBER_userName']) : "";
        $arParams['SBER_password'] = trim($_REQUEST['SBER_password']) ? trim($_REQUEST['SBER_password']) : "";
        $arParams['sber_yourSecretToken'] = trim($_REQUEST['sber_yourSecretToken']) ? trim($_REQUEST['sber_yourSecretToken']) : "";
        $arParams['SBER_returnUrl'] = trim($_REQUEST['SBER_returnUrl']) ? trim($_REQUEST['SBER_returnUrl']) : "";
        $arParams['SBER_failUrl'] = trim($_REQUEST['SBER_failUrl']) ? trim($_REQUEST['SBER_failUrl']) : "";
        $arParams['SBER_sessionTimeoutSecs'] = trim($_REQUEST['SBER_sessionTimeoutSecs']) ? trim($_REQUEST['SBER_sessionTimeoutSecs']) : "";
        $arParams['SBER_systemcode'] = trim($_REQUEST['SBER_systemcode']) ? trim($_REQUEST['SBER_systemcode']) : "";
        $arParams['SBER_customerDetailsPhone'] = trim($_REQUEST['SBER_customerDetailsPhone']) ? trim($_REQUEST['SBER_customerDetailsPhone']) : "";
        $arParams['SBER_customerDetailsEmail'] = trim($_REQUEST['SBER_customerDetailsEmail']) ? trim($_REQUEST['SBER_customerDetailsEmail']) : "";
        $arParams['SBER_customerDetailsFullName'] = trim($_REQUEST['SBER_customerDetailsFullName']) ? trim($_REQUEST['SBER_customerDetailsFullName']) : "";

        return serialize($arParams);

    }

    //получение кнопки для оплаты
    public static function getPayButton($orderId){

        if(!$orderId) return '';

        $res = \Mlife\Asz\OrderTable::getList(array("select"=>array("*"),"filter"=>array("ID"=>$orderId)));
        if($dataAr = $res->Fetch()){

            $paymentId = $dataAr["PAY_ID"];
            $paramArray = self::getParamsFromBase($paymentId);

            if($paramArray['ASZ_STATUS']=='all'){
                $right = true;
            }else{
                if($dataAr['STATUS']){
                    if(in_array($dataAr['STATUS'],explode(',',$paramArray['ASZ_STATUS']))) {
                        $right = true;
                    }else{
                        $right = false;
                    }
                }else{
                    $right = false;
                }
            }
            if(!$right) return "";

            $httpClient = new \Bitrix\Main\Web\HttpClient();
            $url = 'https://securepayments.sberbank.ru/payment/rest/';
            if($paramArray['TEST_MODE'] == 'Y'){
                $url = 'https://3dsec.sberbank.ru/payment/rest/';
            }


            $userProps = array();
            $uRes = \Mlife\Asz\OrderpropsValuesTable::getList(array('select'=>array('*'),'filter'=>array('UID'=>$dataAr['USERID'])));
            while($arProp = $uRes->Fetch()){
                $userProps[$arProp['PROPID']] = $arProp['VALUE'];
            }

            $basket = array();
            $resb = \Mlife\Asz\BasketTable::getList(
                array(
                    'select' => array("*"),
                    'filter' => array("ORDER_ID"=>$dataAr['ID'])
                )
            );
            $orderSumm = 0;
            while($arResBasket = $resb->Fetch()){
                $basket[] = $arResBasket;
                $orderSumm += round(($arResBasket["PRICE_VAL"]-$arResBasket["DISCOUNT_VAL"])*$arResBasket["QUANT"]*100);
            }

            $postPrams = array(
                'userName'=>$paramArray['SBER_userName'],
                'password'=>$paramArray['SBER_password'],
                'orderNumber'=>$dataAr['ID'],
                'amount'=>$dataAr['PRICE']*100,
                'returnUrl'=>$paramArray['SBER_returnUrl'].'&ORDER_ID='.$dataAr['ID'],
                'failUrl'=>$paramArray['SBER_failUrl'].'&ORDER_ID='.$dataAr['ID'],
                'sessionTimeoutSecs'=>$paramArray['SBER_sessionTimeoutSecs'],
                'orderBundle'=>array(
                    'customerDetails'=>array(),
                    'cartItems'=>array()
                )
            );
            $postPrams['orderBundle']['orderCreationDate'] = date("c",$dataAr['DATE']);

            if($paramArray['SBER_customerDetailsPhone'] && $userProps[$paramArray['SBER_customerDetailsPhone']]){
                $postPrams['orderBundle']['customerDetails']['phone'] = $userProps[$paramArray['SBER_customerDetailsPhone']];
                $postPrams['jsonParams']['phone'] = $userProps[$paramArray['SBER_customerDetailsPhone']];
            }
            if($paramArray['SBER_customerDetailsEmail'] && $userProps[$paramArray['SBER_customerDetailsEmail']]){
                $postPrams['orderBundle']['customerDetails']['email'] = $userProps[$paramArray['SBER_customerDetailsEmail']];
                $postPrams['jsonParams']['email'] = $userProps[$paramArray['SBER_customerDetailsEmail']];
            }
            if($paramArray['SBER_customerDetailsFullName'] && $userProps[$paramArray['SBER_customerDetailsFullName']]){
                $postPrams['orderBundle']['customerDetails']['fullName'] = $userProps[$paramArray['SBER_customerDetailsFullName']];
            }
            if(is_array($postPrams['jsonParams'])){
                $postPrams['jsonParams'] = json_encode($postPrams['jsonParams']);
            }
            foreach($basket as $row){
                $rowApi = array(
                    'positionId'=>$row['ID'],
                    'name'=>$row['PROD_NAME'],
                    'itemDetails'=>$row['PROD_DESC'],
                    'quantity'=>array(
                        'value'=>$row['QUANT'],
                        'measure'=>'шт',
                    ),
                    'itemPrice'=>round(($row["PRICE_VAL"]-$row["DISCOUNT_VAL"]),2)*100,
                    'itemAttributes' =>array(
                        'paymentMethod'=>1,
                        'paymentObject'=>1
                    )
                );
                if($paramArray['FISK_MODE'] != 'Y'){
                    unset($rowApi['itemAttributes']);
                }
                $postPrams['orderBundle']['cartItems'][] = $rowApi;
            }
            if($paramArray['FISK_MODE'] != 'Y'){
                unset($postPrams['orderBundle']);
            }

            $uRes = \Mlife\Asz\OrderpropsValuesTable::getList(array(
                'select'=>array('*'),
                'filter'=>array('UID'=>$dataAr['USERID'], 'PROPID'=>$paramArray['SBER_systemcode'])
            ))->fetch();

            if($uRes['VALUE']){
                return '<a class="btn btn-primary" href="'.$uRes['VALUE'].'" target="_blank">оплатить заказ</a>';
            }

            $res = $httpClient->post($url.'register.do',$postPrams);
            if(!$res) return 'Платежная система выдала пустой результат. Обратитесь к менеджеру интернет магазина.';

            $resAr = json_decode($res,true);
            if($resAr['formUrl']){
                if(!$uRes){
                    \Mlife\Asz\OrderpropsValuesTable::add(array(
                        'UID'=>$dataAr['USERID'],
                        'PROPID'=>$paramArray['SBER_systemcode'],
                        'VALUE'=>$resAr['formUrl']
                    ));

                }else{
                    \Mlife\Asz\OrderpropsValuesTable::update(array('ID'=>$uRes['ID']),array(
                        'UID'=>$dataAr['USERID'],
                        'PROPID'=>$paramArray['SBER_systemcode'],
                        'VALUE'=>$resAr['formUrl']
                    ));
                }
                return '<a class="btn btn-primary" href="'.$resAr['formUrl'].'" target="_blank">оплатить заказ</a>';
            }else{
                return $resAr['errorMessage'];
            }

            //echo'<pre>';print_r($resAr);echo'</pre>';
            //echo'<pre>';print_r($postPrams);echo'</pre>';

        }

        return "";
    }

    public static function checkPayToApi($order){

        $paymentId = $order["PAY_ID"];
        $paramArray = self::getParamsFromBase($paymentId);

        $httpClient = new \Bitrix\Main\Web\HttpClient();
        $url = 'https://securepayments.sberbank.ru/payment/rest/';
        if($paramArray['TEST_MODE'] == 'Y'){
            $url = 'https://3dsec.sberbank.ru/payment/rest/';
        }


        $postPrams = array(
            'userName'=>$paramArray['SBER_userName'],
            'password'=>$paramArray['SBER_password'],
            'orderNumber'=>$order['ID'],
        );

        $res = $httpClient->post($url.'getOrderStatusExtended.do',$postPrams);
        if(!$res) return 'Платежная система выдала пустой результат. Обратитесь к менеджеру интернет магазина.';

        $resAr = json_decode($res,true);

        if($resAr['orderStatus'] == 2){
            if($paramArray["ASZ_STATUS_S"]>0){
                //обновление статуса
                $res = \Mlife\Asz\OrderTable::update($order["ID"],array("STATUS"=>$paramArray["ASZ_STATUS_S"]));
            }
            return 'Заказ успешно оплачен.';
        }

        //echo'<pre>';print_r($resAr);echo'</pre>';

    }

    public static function checkPay($order){

        $paymentId = $order["PAY_ID"];
        $paramArray = self::getParamsFromBase($paymentId);

        $str = '';
        foreach($_REQUEST as $k=>$v){
            if($k ==  'checksum') continue;
            $str .= $k.';'.$v.';';
        }
        $key = $paramArray['sber_yourSecretToken'];
        $hmac = hash_hmac ( 'sha256' , $str , $key);

    }

}